#!/bin/sh

export CHROME_PATH=/usr/bin/brave-browser
IDXFILE="README.md"

printf "# Slides\n\nBased on Marp\n\n" > "$IDXFILE"

for i in *.md
do
  if [ "$i" != "README.md" ] && [ "$i" != "index.md" ]
  then

    # Generate links in index
    echo "- [$i]($i)" |sed 's/\.md/.pdf/g' >> $IDXFILE
    echo "- [$i]($i)" |sed 's/\.md/.html/g' >> $IDXFILE

    # Marp it
    if [ -f "/usr/bin/marp" ]
    then
      basename=${i%.*}
      echo "$i - $basename"
      marp  "$i" --allow-local-files -o "${basename}.pdf" --pdf
      marp  "$i" --allow-local-files -o "${basename}.html" --html
    fi
  fi

done

[ -f "/usr/bin/marp" ] && marp  "$IDXFILE" --allow-local-files -o "index.html" --html
