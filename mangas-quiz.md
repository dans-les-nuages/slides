---
marp                    : true
backgroundImage         : url('images/mangas-quiz/fond-blanc.jpg')
color                   : #000080

---
<style>   
@import url('https://fonts.cdnfonts.com/css/komika-axis');
@font-face {
  font-family: "Komika Axis";
  src: url('https://fonts.cdnfonts.com/css/komika-axis');
}
h1, h2, h3 { font-family: "Komika Axis", sans-serif; }
h1, h2 { text-align: right !important;}
h1 { font-size: 2.8rem;   }
h2 { font-size: 2rem;   }
h3 { font-size: 1.5rem; }
</style>


<!-- _backgroundImage: url('images/mangas-quiz/fond.jpg') -->
# Quiz

---
### Retrouve le nom du manga à partir de l'image

---
### ?

![bg left:60%](images/mangas-quiz/1.png)

---
### Dr Slump 

De Akira Toriyama 1955-2024

Auteur également de la série Dragon Ball

![bg left:60%](images/mangas-quiz/1.png)

---
### ?

![bg left:60%](images/mangas-quiz/2.jpg)

---
### My hero academia

![bg left:60%](images/mangas-quiz/2.jpg)

---
### ?

![bg left:60%](images/mangas-quiz/3.jpg)

---
### Chobits

Du collectif Clamps

![bg left:60%](images/mangas-quiz/3.jpg)

---
### ?

![bg left:60%](images/mangas-quiz/4.jpg)

---
### Captain Tsubasa 

(Olive et Tom)

![bg left:60%](images/mangas-quiz/4.jpg)

---
### ?

![bg left:60%](images/mangas-quiz/5.png)

---
### Sailor Moon

![bg left:60%](images/mangas-quiz/5.png)

---
### ?

![bg left:60%](images/mangas-quiz/6.jpg)

---
### The promised neverland

![bg left:60%](images/mangas-quiz/6.jpg)

---
### ?

![bg left:60%](images/mangas-quiz/7.jpg)

---
### Yona princesse de l'aube

![bg left:60%](images/mangas-quiz/7.jpg)

---
### ?

![bg left:60%](images/mangas-quiz/8.jpg)

---
### Ao Ashi

![bg left:60%](images/mangas-quiz/8.jpg)

---
### ?

![bg left:60%](images/mangas-quiz/9.jpg)

---
### Chocola & Vanilla

![bg left:60%](images/mangas-quiz/9.jpg)

---
### ?

![bg left:60%](images/mangas-quiz/10.png)

---
### L'atelier des sorciers

![bg left:60%](images/mangas-quiz/10.png)

---
### ?

![bg left:60%](images/mangas-quiz/11.png)

---
### Full metal alchemist

![bg left:60%](images/mangas-quiz/11.png)

---

- Je suis un shonen
- Je suis adapté en anime
- L'histoire suit le périple de Kamado Tanjirō qui cherche un 
 moyen de rendre sa petite sœur Nezuko de nouveau humaine 
 après sa transformation en …

---
### ?

![bg left:60%](images/mangas-quiz/12.jpg)

---
### Demon Slayer

![bg left:60%](images/mangas-quiz/12.jpg)

---
### ?

![bg left:60%](images/mangas-quiz/13.jpg)

---
### Card Captor Sakura

![bg left:60%](images/mangas-quiz/13.jpg)

---
### ?

![bg left:60%](images/mangas-quiz/14.jpg)

---
### Detective Conan

![bg left:60%](images/mangas-quiz/14.jpg)

---
### Citez-moi toutes les séries de Dragon Ball

---
### Citez-moi toutes les séries de Dragon Ball

Dans l'ordre :

- Dragon Ball
- Dragon Ball Z
- Dragon Ball Kai
- Dragon Ball GT
- Dragon Ball Super

---
### Citez-moi toutes les séries de Dragon Ball

L'adaptation du manga en anime se divise en deux parties distinctes :

- Dragon Ball s'intéresse à la jeunesse du personnage principal
- Dragon Ball Z relate les aventures de son fils, bien que Goku 
 reste un personnage de premier plan, souvent à l'origine du dénouement.

---
### Savez-vous comment s’appellent ces déguisements ?

![bg left:60%](images/mangas-quiz/cosplay.png)

---
### Le cosplay

![bg left:60%](images/mangas-quiz/cosplay.png)

