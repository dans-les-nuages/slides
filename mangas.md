---
marp                    : true
backgroundImage         : url('images/mangas/fond-blanc.jpg')
color                   : #000080

---
<style>   
@import url('https://fonts.cdnfonts.com/css/komika-axis');
@font-face {
  font-family: "Komika Axis";
  src: url('https://fonts.cdnfonts.com/css/komika-axis');
}
h1, h2, h3 { font-family: "Komika Axis", sans-serif; }
h1, h2 { text-align: right !important;}
h1 { font-size: 2.8rem;   }
h2 { font-size: 2rem;   }
h3 { font-size: 1.5rem; }
</style>


<!-- _backgroundImage: url('images/mangas/fond.jpg') -->
# Les mangas

---
<!-- _backgroundImage: url('images/mangas/fond.jpg') -->
## Types de manga

---
### Shojo

Publications pour les jeunes filles, souvent des histoires romantiques dans des contextes comme une école, un club sportif :

*Fruits baskets, Nana, La rose de Versailles*

![bg right:60% 80%](images/mangas/fruits-basket.jpg)
![bg 82%](images/mangas/nana.jpg)
![bg 90%](images/mangas/la-rose-de-versailles.jpg)

---
### Shonen

Publications pour les garçons de 7 à 18 ans, plus grande partie de la production de mangas, sont les séries mettent en scène des héros ados auxquels le lecteur peut s’identifier :

*Dragon ball, One piece, Naruto, GTO*

![bg right:60% 95%](images/mangas/dragon-ball.png)
![bg 95%](images/mangas/one-piece.jpg)
![bg 95%](images/mangas/naruto.jpg)
![bg 95%](images/mangas/gto.jpg)

---
### Seinen

Publications à destination des adultes :

*Berserk, 20th century boys*

![bg right:70% 90%](images/mangas/berserk.jpg)
![bg 90%](images/mangas/20th-century-boy.jpg)
![bg 90%](images/mangas/akira.jpg)

---
### Kodomo

Publications pour jeunes enfants (6-11 ans) :

*Pokémon, Doraemon, Astro boy, Chi une vie de chat*

![bg right:60% 90%](images/mangas/pokemon.jpg)
![bg 90%](images/mangas/doraemon.jpg)
![bg 90%](images/mangas/astroboy.jpg)
![bg 90%](images/mangas/chi.jpg)

---
<!-- _backgroundImage: url('images/mangas/fond.jpg') -->
## Histoire

---
Au Japon, au VIIIe siècle, la peinture devient une forme d’expression artistique très en vogue à la cour, sous forme de rouleaux peints :

Les **emakimonos** (véritables fresques) longs de 15m, racontent de véritables histoires

![bg right:60% 90%](images/mangas/emakimonos1.jpg)
![bg right:60% 90%](images/mangas/emakimonos2.jpg)

---
Au XII siècle un moine bouddhiste dessine le célèbre Rouleau des oiseaux et des animaux qui met en scène des fables animalières.

Il étonne encore par sa similarité avec le manga contemporain. Ses dessins ont été animés en 2016 par les studios Ghibli.

La présence de certains Yôkai créatures mythlogiques liées au folflore japonais ont influencé beaucoup d’auteurs d’aujourd’hui.

![bg right:40% 100%](images/mangas/estampes.jpg)

---
### Hokusai, maître de l'estampe

A l’ère Edo (1603-1868), émergence des estampes dans les milieux aisés.

Le maitre de l’estampe est Hokusai (1760-1849) considéré comme l’ancêtre du manga contemporain.

En 1814 il publie des carnets de croquis qui illustrent la vie quotidienne des Japonais.

Son style est simple et proche de la caricature.
Il intitule un de ces recueils « Hokusai manga ».

---
### La Grande Vague de Kanagawa

Plus connue sous le nom de **La Vague**, est une célèbre estampe japonaise du peintre japonais, Hokusaï, publiée en 1830 pendant l’époque d’Edo.

![bg right:60% ](images/mangas/la-vague.png)

---
### Le manga au XXe siècle

En 1914, un éditeur crée le shonen club un magazine qui propose des BD pour jeunes garçons puis en 1920 pour jeunes filles

Les premiers héros de manga naissent dans les années 1930

Mais c’est après la seconde guerre mondiale que le manga explose:
Découverte des comic strips sous l’occupation américaine et qui devient une source d’inspiration très importante.

---
### Le manga au XXe siècle

Dans les années 1930 le manga est nationaliste et militariste.

Après-guerre il offre des histoires de survie d’adolescents dans un monde en ruine.

Dans les années 1960 ils voient fleurir des récits d’émancipation de jeunes filles, tandis que le magazine shonen jump multiplie les histoires de réussite de jeunes héros aux valeurs positives.

---
### Le manga moderne

C’est parce qu’il s’adresse à tous les membres de la société et s’empare de la vie quotidienne des japonais que le manga est devenu ce qu’il est aujourd’hui :

Un reflet du japon, qui en dévoile toutes les nuances.

---
<!-- _backgroundImage: url('images/mangas/fond.jpg') -->
## Auteurs

---
### Osamu Tezuka (1928-1989)

Equivalent d’Hergé et Walt Disney à lui tout seul
170 000 planches, 60 œuvres audiovisuelles animées.

En 1962 créateur du studio d’animation Mushi production qui transforme son personnage d’Astro boy en anime puis en 1968 il créé Tezuka Productions

Considéré comme « le dieu du manga »

![bg right:40% ](images/mangas/tezuka.jpg)

---
### Osamu Tezuka (1928-1989)

Son œuvre est une grande diversité (science fiction, fresques historiques, thrillers, drames, contes fantastiques puis biographiques avec la vie de bouddha)

Il créé du mouvement et de la vitesse dans ses dessins. Il ajoute à ses personnages des grands yeux inspiré de bambi des studios Disney.

En 1952 il créé Astro boy. Beaucoup de futurs jeunes talents s’inspirent de Tezuka.

![bg right:30% 95%](images/mangas/le-roi-leo.png)

---
<!-- _backgroundImage: url('images/mangas/fond.jpg') -->
## Caractéristiques

---
Le manga est d’abord édité via des magazines de prépublication sur du papier très bon marché. On compte environ 300 magazines de pré publication

Chaque magazine de prépublication vise un public défini par son âge son sexe.

Ces magazines se trouvent partout (gares, kombini : épiceries ouvertes 24h/24 )

Leur prix très bas et leur variété font du manga un art populaire.

![bg right:50% ](images/mangas/manga.png)

---

- L’ellipse entre chaque case qu’on voit beaucoup dans le style de bd franco-belge et peu en manga
- Le rythme est plus rapide car il n’y pas la contrainte des 48 pages avec la bd franco-belge. Les auteurs multiplient les dessins intermédiaires. Ça donne un découpage très cinématographique
- Sens du lecture  
- L’histoire prédomine sur le dessin
- noir et blanc (pour des questions de coûts)

---
 
- Le dessin: visages ovales, grands yeux, petites bouches, expression exagérés et proches de la caricature
- Le rythme des parutions impose un rythme de travail aux mangakas très intenses
- Le nom est formé de man (dérisoire, imprécision, légèreté) et ga (dessin). Ils désignent des croquis faits en vitesse et sans lien entre eux.
- Le ka de mangaka désigne un expert en quelque chose

---
<!-- _backgroundImage: url('images/mangas/fond.jpg') -->
## En France

---
Goldorak a été la 1ere série animée nippone diffusée en 1978 sur nos écrans bien avant dragon ball (diffusé en 1988)

![bg right:40% 90%](images/mangas/goldorak.jpg)
![bg right:40% 90%](images/mangas/dragon-ball.png)

Diffusion au club dorothée de nombreuses séries animées. Parution d’Akira en France.

Depuis la crise du covid, les ventes ont explosé en France, puis grâce aussi au pass culture.

---
**One piece** est le manga de tous les records : le plus vendus , rythme de parution éffréné
 
La France est le 2e pays le plus consommateur de manga juste derrière le Japon avec les séries les plus vendues : **One piece**, **Fairy tail** et **My hero academia**

![bg right:40% ](images/mangas/one-piece.jpg)

---
### Quelques séries françaises

- Dreamland
- Everdark
- Radiant

![bg right:65% ](images/mangas/dreamland.png)
![bg](images/mangas/everdark.jpg)
![bg](images/mangas/radiant.jpg)

---
<!-- _backgroundImage: url('images/mangas/fond.jpg') -->
## Le manga dans d'autres pays

---
### Manga coreen (Manhwa)

- Solo Leveling
- White blood

![bg right:60%](images/mangas/solo-leveling.jpg)
![bg](images/mangas/white-blood.jpg)

---
### Manga chinois (Manhua)

- La balade de Yaya
- Le monde de Zhou Zhou

![bg right:60%](images/mangas/yaya.jpg)
![bg](images/mangas/zhou-zhou.jpg)

---
<!-- _backgroundImage: url('images/mangas/fond.jpg') -->
## Anime

---

Studio Ghibli a été fondé en 1985 par Takahata et Miyazaki avec une 20 de longs métrages ; thèmes : épiques, émouvants, électriques, écologiques
 
- 1988 : Mon voisin Totoro
- 1988 : Le tombeau des lucioles
- Studio chizu : 
  + 2012 : Les enfants loups
  + 2015 : le garçon et la bête
- 2017 : Mary et la fleur de sorcière
- ...

![bg right:50% 90%](images/mangas/les-enfants-loups.jpg)
![bg right:50% 90%](images/mangas/le-garcon-et-la-bete.webp)
![bg right:50% 90%](images/mangas/le-chateau-dans-le-ciel.jpg)

---
### Des mangas adaptés en anime

- One Piece
- Demon slayer

![bg right:60%](images/mangas/one-piece.jpg)
![bg](images/mangas/one-piece-tv.jpg)
![bg](images/mangas/demon-slayer.jpg)

---
<!-- _backgroundImage: url('images/mangas/fond.jpg') -->
## Quiz

---
### Savez-vous comment s’appellent ces déguisements ?

![bg right:60%](images/mangas/cosplay.png)

---
### Le cosplay

![bg right:60%](images/mangas/cosplay.png)

